#!/bin/bash


audio=true
vidstab=false

while getopts ":vs" opt; do
  case $opt in
    v)
      audio=false
      ;;
    s)
      vidstab=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
shift $((OPTIND -1))

if [[ $(uname -s) == "Linux" ]]
then
    ffmpeg_exe="ffmpeg"
    devnull="/dev/null"
else
    ffmpeg_exe="/cygdrive/e/fraps/processed/warthunder/ffmpeg.exe"
    devnull="NUL"
fi

if $audio
then
    audioargs="-b:a 192k -c:a libopus"
else
    audioargs="-an"
fi

if $vidstab
then
    echo "USING VIDSTAB"
    vidstabargs1="vidstabdetect=shakiness=5:show=1"
    vidstabargs2="vidstabtransform,unsharp=5:5:0.8:3:3:0.4"
else
    vidstabargs1=""
    vidstabargs2=""
fi

morefilters="scale=w=1920:h=1080:force_original_aspect_ratio=decrease"
vp9="-c:v libvpx-vp9 -b:v 10000k -f webm -threads 8 -tile-columns 6 -frame-parallel 1"

for infile in "$@"
do
    basefile="${infile%.*}"
    outfile="${basefile}.webm"
    
    if ! [ -e $outfile ]
    then
        $ffmpeg_exe -y -i "$infile" $vp9 -pass 1 -speed 4 -vf "$vidstabargs1, $morefilters" -an $devnull
        
        $ffmpeg_exe    -i "$infile" $vp9 -pass 2 -speed 1 -auto-alt-ref 1 -lag-in-frames 25 -g 30 -vf "$vidstabargs2, $morefilters" $audioargs "$outfile"
    fi
done
